attiny-wdt: Turn an ATtiny9 uC into a watchdog for another processor
====================================================================

The ATtiny9 is a small, ultra low cost microcontroller available in SOT-23
package.  It is lower cost (and currently better available) than dedicated
hardware watchdog ICs.

So this project turns the ATtiny9 into a very simple "hardware watchdog IC"
for another processor/CPU.

The watchdog starts counting immediately from power-on and will trigger a reset
of the target system after 67s, unless the target system keeps refreshing the
watchdog via a low edge on another signal.

Status
------

The device has been developed and tested, and has been deployed in production in
hundreds of systems.

GIT Repository
--------------

You can clone from the official repository using

	git clone https://gitea.osmocom.org/electronics/attiny-wdt

There is a web interface at <https://gitea.osmocom.org/electronics/attiny-wdt>
