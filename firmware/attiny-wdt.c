#include <avr/io.h>
#include <avr/interrupt.h>

/* This turns an ATTiny9 into a very simplistic external watchdog for another processor,
 * in the current case an Raspberry Pi.
 *
 * The idea is that there is a timer running for about 15 minutes. If the timer expires,
 * PB1 (default high) will turn low for about 1s, before turning back high again.
 *
 * This can be prevented by causing a falling edge on PB2 before the 15 minutes expire.
 */

/* GPIO assignment:
 * 	- PB1: OC0B: output triggering the (low-active) reset pin of the Raspi
 * 	- PB2: INT0: falling-edge interrupt to trigger the watchdog
 */

/* Example Clock configurations:
 *
 * fOSC is always 8MHz (uncalibrated)
 * | Prescaler | CLKPS3:0 | f_clk_io | Tick freq | Max intv |
 * | 8         |     0011 |    1 MHz |  1.024 ms |  67.1 s  |
 * | 128       |     0111 | 62.5 kHz | 16.384 ms |  1073 s  |
 */

//#define DEVELOPMENT

#ifdef DEVELOPMENT
/* development operation: 15s timeout, 1s pulse with */
#define EXPIRE_AFTER_s		15U		/* 5 seconds */
#define PULSE_DURATION_s	1U
#else
/* normal operation: 15min timeout, 1s pulse with */
#define EXPIRE_AFTER_s		(15U*60U)	/* you can adjust this */
#define PULSE_DURATION_s	1U		/* you can adjust this */
#endif


/* INT0 (PB2 falling edge) */
ISR(INT0_vect)
{
	/* reset the counter to zero */
	TCNT0H = 0;
	TCNT0L = 0;
}

#define	TICK_RATE_Hz		61U		/* actually 61.03, but well... */
#define TRIGGER_TICKS		(PULSE_DURATION_s * TICK_RATE_Hz)
#define TRIGGER_VALUE		(EXPIRE_AFTER_s * TICK_RATE_Hz)
#define TOP_VALUE 		(TRIGGER_VALUE + TRIGGER_TICKS)

int main(void)
{
	const uint8_t wgm = 15; /* fast PWM; TOP=OCR0A */

	/* Enable pull-up at all pins except PB1 */
	PUEB = _BV(PB0) | _BV(PB2) | _BV(PB3);

	/* PORTB is initialized to all-zero at reset; This makes sure
	 * PB1 will output low when we switch it to output */

	/* Set PB0/2/3 as input; PB1 as output */
	DDRB = _BV(PB1);

	/* Enable INT0 on falling edge of PB2 */
	EICRA = _BV(ISC01);
	EIMSK = _BV(INT0);

	/* Set system clock prescaler to 128, resulting in 8MHz/128 = 62.5 kHz */
	CCP = 0xD8;
	CLKPSR = 0x07;

	/* In non-inverting Compare Output mode, the Output Compare (OC0B)
	 * is cleared on the compare match between TCNT0 and OCR0B, and set at BOTTOM */

	/* Set OC0B on compare (drives T1 to pull GLOBAL_EN low); Clear OC0B at BOTTOM */
	TCCR0A = _BV(COM0B1) | _BV(COM0B0) | (wgm & 3);

	/* ICES0=0 (input capture falling edge)
	 * ICNC0=0 (no noise canceller)
	 * clock divider = 1024; this means counting at 61 Hz */
	TCCR0B = ((wgm >> 2) << 3) | _BV(CS02) | _BV(CS00);

	/* TOP = OCR0A; high byte must be written before low byte */
	OCR0AH = TOP_VALUE >> 8;
	OCR0AL = TOP_VALUE & 0xff;

	/* TRIGGER = OCR0B; high byte must be written before low byte */
	OCR0BH = TRIGGER_VALUE >> 8;
	OCR0BL = TRIGGER_VALUE & 0xff;

	/* globally enable interrupts */
	sei();

	while (1) {
	}
}
